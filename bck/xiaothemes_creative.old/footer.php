		<!-- ALERT BOX --> 
	<div id="alert">
		<span class="title1">This is a useful alert message:</span><br/>
		<span class="title2">Мы уже почти все сделали</span>
		<span class="close"><a href="#" id="close_bt" title="Close"><img src="<?php echo get_template_directory_uri(); ?>/images/close.png" alt="close"/></a></span>
	</div>
		
	
		<!-- THUMBNAILS - FULLSCREEN GALLERY --> 
		<div id="thumbs">
			<ul>
				<li><a href="http://onesummerago.com/wp-content/uploads/2012/06/bgi_6.jpg" class="rollover" data-valign="top" data-align="right"><img src="http://onesummerago.com/wp-content/uploads/2012/06/thumbnail_6.jpg" alt="thumbnail"/></a></li>
				<li><a href="http://onesummerago.com/wp-content/uploads/2012/06/bgi_1.jpg" class="rollover" data-valign="top" data-align="right"><img src="http://onesummerago.com/wp-content/uploads/2012/06/thumbnail_1.jpg" alt="thumbnail"/></a></li>
				<li><a href="http://onesummerago.com/wp-content/uploads/2012/06/bgi_2.jpg" class="rollover" data-valign="top" data-align="right"><img src="http://onesummerago.com/wp-content/uploads/2012/06/thumbnail_2.jpg" alt="thumbnail"/></a></li>
				<li><a href="http://onesummerago.com/wp-content/uploads/2012/06/bgi_3.jpg" class="rollover" data-valign="top" data-align="right"><img src="http://onesummerago.com/wp-content/uploads/2012/06/thumbnail_3.jpg" alt="thumbnail"/></a></li>
				<li><a href="http://onesummerago.com/wp-content/uploads/2012/06/bgi_4.jpg" class="rollover" data-valign="top" data-align="right"><img src="http://onesummerago.com/wp-content/uploads/2012/06/thumbnail_4.jpg" alt="thumbnail"/></a></li>
				<li><a href="http://onesummerago.com/wp-content/uploads/2012/06/bgi_5.jpg" class="rollover" data-valign="top" data-align="right"><img src="http://onesummerago.com/wp-content/uploads/2012/06/thumbnail_5.jpg" alt="thumbnail"/></a></li>
				<li><a href="http://onesummerago.com/wp-content/uploads/2012/06/bgi_7.jpg" class="rollover" data-valign="top" data-align="right"><img src="http://onesummerago.com/wp-content/uploads/2012/06/thumbnail_7.jpg" alt="thumbnail"/></a></li>
			</ul>
			<img id="slideshow" class="playpause" title="Play slideshow" src="<?php echo get_template_directory_uri(); ?>/images/play.png" alt="slideshow"/>
		</div>
		

	<!-- SOCIAL NETWORKS -->
	<div id="social">	
		<ul>
			<li><a href="#" target="_blank" title="Vkontakte"><img src="<?php echo get_template_directory_uri(); ?>/images/social/vkontakte.png" alt=" "/></a></li>
			<li><a href="#" target="_blank" title="Facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/social/facebook.png" alt=" "/></a></li>
			<li><a href="#" target="_blank" title="Twitter" ><img src="<?php echo get_template_directory_uri(); ?>/images/social/twitter.png" alt=" "/></a></li>
			<li><a href="#" target="_blank" title="Google Plus" ><img src="<?php echo get_template_directory_uri(); ?>/images/social/gplus.png" alt=" "/></a></li>
			<li><a href="#" target="_blank" title="Vimeo" ><img src="<?php echo get_template_directory_uri(); ?>/images/social/vimeo.png" alt=" "/></a></li>
			<li><a href="#" target="_blank" title="Youtube" ><img src="<?php echo get_template_directory_uri(); ?>/images/social/youtube.png" alt=" "/></a></li>
		</ul>
	</div>
	
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/load-posts.js'></script>
	<?php wp_footer(); ?>
</body>
</html>